def first_anagram?(word1, word2) #n!
  perms = word1.chars.permutation.to_a
  perms = perms.map(&:join)
  perms.include?(word2)
end

def second_anagram?(word1, word2) #n^2
  word1.chars.each do |char|
    if word1.include?(char) && word2.include?(char)
      word1.sub!(char, "")
      word2.sub!(char, "")
    end
  end
  (word1.length == 0) && (word2.length == 0)
end

# p second_anagram?("gizmo", "sally")
# p second_anagram?("elvis", "lives")


def third_anagram?(word1, word2) #n^2
  word1.chars.sort.join == word2.chars.sort.join
end

# p third_anagram?("gizmo", "sally")
# p third_anagram?("elvis", "lives")

def fourth_anagram?(word1, word2) #n
  hash = Hash.new(0)
  word1.chars.each { |char| hash[char] += 1 }
  word2.chars.each { |char| hash[char] -= 1 }
  hash.values.all?(&:zero?)
end

p fourth_anagram?("gizmo", "sally")
p fourth_anagram?("elvis", "lives")
