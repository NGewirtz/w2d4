require 'byebug'

def two_sum?(arr, target) #n^2
  i = 0
  while i < arr.length
    j = i + 1
    while j < arr.length
      return true if arr[i] + arr[j] == target
      j += 1
    end
    i += 1
  end
  false
end


# p two_sum?(arr, 6) # => should be true
# p two_sum?(arr, 10)

def okay_two_sum?(arr, target) #n log n
  arr = arr.sort
  arr.each_with_index do |el, idx|
    compliment = target - el
    sub_arr = arr[0...idx] + arr[idx+1..-1]
    return true if b_search(sub_arr, compliment)
  end
  false
end

def b_search(arr, target)
  return false if arr.empty?
  mid = arr.length / 2
  left = arr.take(mid)
  right = arr.drop(mid)
  case target <=> arr[mid]
  when -1
    b_search(left, target)
  when 0
    return true
  when 1
    b_search(right[1..-1], target)
  end
end


# p okay_two_sum?(arr, 6) # => should be true
# p okay_two_sum?(arr, 10)
arr = [0, 1, 5, 7]

def best_two_sum(arr, target) #n

  sum_hash = Hash.new(0)

  arr.each do |el|
    sum_hash[el] += 1

  end

  arr.each do |el|
    compliment =  target - el

    if compliment == el
      return true if sum_hash[el] > 1
    else
      return true if sum_hash[compliment] > 0
    end

  end
  false
end


p best_two_sum(arr, 6) # => should be true
p best_two_sum(arr, 10)
