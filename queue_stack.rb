class MyQueue
  def initialize
    @store = []
  end

  def enqueue(arg)
    @store.push(arg)
  end

  def dequeue
    @store.shift
  end

  def peek
    @store.first
  end

  def size
    @store.count
  end

  def empty?
    @store.empty?
  end

end

class MyStack
  attr_accessor :store
  def initialize
    @store = []
  end

  def pop
    @store.pop
  end

  def push(arg)
    @store.push(arg)
  end

  def peek
    @store.last
  end

  def size
    @store.count
  end

  def empty?
    @store.empty?
  end

end

class StackQueue < MyStack

  attr_accessor :max, :min, :in_stack
  def initialize
    @in_stack = MyStack.new
    @out_stack = MyStack.new
  end

  def enqueue(arg)
    if @in_stack.empty?
      cur_max = arg
      cur_min = arg
    else
      top = @in_stack.peek
      # debugger
      cur_max = arg > top[:max] ? arg : top[:max]
      cur_min = arg < top[:min] ? arg : top[:min]
    end
    hash = { num: arg, max: cur_max, min: cur_min }
    @in_stack.push(hash)
  end

  def dequeue
    if @out_stack.empty?
      @in_stack.size.times do
        @out_stack.push(@in_stack.pop)
      end
    end
    @out_stack.pop
  end

  def size
    @in_stack.size + @out_stack.size
  end

  def empty?
    @in_stack.empty? && @out_stack.empty?
  end

  def peek
    if @out_stack.empty?
      @in_stack.store.last
    else
      @out_stack.store.last
    end
  end

end

class MinMaxStackQueue < StackQueue
end
