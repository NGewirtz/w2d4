require 'byebug'
require_relative "queue_stack"

def windowed_max_range(array, window_size)
  current_max_range = nil

  array.each_with_index do |el, idx|
    break if idx + window_size > array.length
    window = array[idx...idx+window_size]

    max = window.max
    min = window.min
    # debugger
    current_max_range = (max - min) if current_max_range.nil? || current_max_range < (max-min)

  end
  current_max_range
end


def windowed_max_range_with_queue(array, window_size)
  current_max_range = nil
  q = MinMaxStackQueue.new
  window_size.times do |idx|
    q.enqueue(array[idx])
    current_max_range = q.in_stack.peek[:max] - q.in_stack.peek[:min]
  end

  (array.length - window_size).times do |idx|
    q.enqueue(array[idx + window_size])
    q.dequeue
    current_max_range = q.peek[:max] - q.peek[:min] if current_max_range < q.peek[:max] - q.peek[:min]
  end

  current_max_range
end




p windowed_max_range_with_queue([1, 0, 2, 5, 4, 8], 2) #== 4 # 4, 8
p windowed_max_range_with_queue([1, 0, 2, 5, 4, 8], 3) #== 5 # 0, 2, 5
p windowed_max_range_with_queue([1, 0, 2, 5, 4, 8], 4) #== 6 # 2, 5, 4, 8
p windowed_max_range_with_queue([1, 3, 2, 5, 4, 8], 5) #== 6 # 3, 2, 5, 4, 8
