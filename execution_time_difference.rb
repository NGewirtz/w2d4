require 'byebug'
list = [-5, -1, -3]

def my_min(list) # O(n^2)
  min = list[0]
  list.each do |el_1|
    list.each do |el_2|
      case el_1 <=> el_2
      when -1
        min = el_1 if min > el_1
      when 0
        min = el_1 if min > el_1
      when 1
        min = el_2 if min > el_2
      end
    end
  end
  min
end

def my_min_fixed(list) #O(n)
  min = list[0]
  (1...list.length).each do |idx|
    min = list[idx] if min > list[idx]
  end
  min
end


def sub_sum(list)
  largest_sub_sum = list
  (0...list.length).each do |idx|
    #(0..idx).each do |idx_2|
    (idx...list.length).each do |idx_2|
      sub_sum = list[idx..idx_2]
      largest_sub_sum = sub_sum if sub_sum.reduce(:+) > largest_sub_sum.reduce(:+)
    end
  end
  largest_sub_sum.reduce(:+)
end

def sub_sum_n(list)
  largest = list.first
  current = list.first

  return list.max if list.all? {|num| num < 0}

  list.drop(1).each do |num|

    current = 0 if current < 0
    current += num
    largest = current if current > largest
  end
  largest
end

p sub_sum_n([2, 3, -6, 7, -6, 7])
p sub_sum(list)
# p my_min(list)
# p my_min_fixed(list)
